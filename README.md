rana-cli
=========

Command line interface to [rana](https://github.com/lnyaa/rana).

This is a fork of the wakatime CLI tool for use with rana.

**Rana is not affiliated with WakaTime.**

Go to http://gitdab.com/lavatech to install the plugin for your text editor or IDE.

## Installation / Forking WakaTime plugins

Note: You shouldn't need to directly use this package unless you're building
your own plugin *or* forking an existing WakaTime plugin for support with Rana.

Since plugins install the WakaTime CLI directly, the recommendation for plugin
forkers is to copy this repository into the plugin's installation of WakaTime CLI.

For example, this would happen on vim-rana:
```
cp -rv path/to/rana-cli/wakatime/* path/to/vim-rana/packages/wakatime/
```

WakaTime plugins can be found in https://wakatime.com/editors.

WakaTime plugins *with Rana support* can be found in https://gitdab.com/lavatech.

## Usage

rana-cli aims to be a seamless replacement of WakaTime CLI. As such, all existing
documentation of WakaTime CLI applies to rana-cli.

 - https://wakatime.com/help/misc/creating-plugin
 - https://wakatime.com/faq

## Configuration

rana-cli, as of right now, has a single extra field in the configuration file:
`api_key`, so that you can switch easily between rana instances (e.g prod/local)

The config file is found on `$WAKATIME_HOME/.wakatime.cfg`, which is the user's
current home directory for UNIXes.

An example config file is as follows. 

```
[settings]
debug = false
api_key = your-api-key
base_url = https://your-rana-instance.com
hide_file_names = false
hide_project_names = false
exclude =
    ^COMMIT_EDITMSG$
    ^TAG_EDITMSG$
    ^/var/(?!www/).*
    ^/etc/
include =
    .*
include_only_with_project_file = false
status_bar_icon = true
status_bar_coding_activity = true
offline = true
proxy = https://user:pass@localhost:8080
no_ssl_verify = false
ssl_certs_file =
timeout = 30
hostname = machinename

[projectmap]
projects/foo = new project name
^/home/user/projects/bar(\d+)/ = project{0}

[git]
disable_submodules = false
```

The default config file may be:

```
[settings]
debug = false
hidefilenames = false
ignore =
    COMMIT_EDITMSG$
    PULLREQ_EDITMSG$
    MERGE_MSG$
    TAG_EDITMSG$

base_url=https://rana.lavatech.top
api_key=awoo
```

Check https://wakatime.com/faq for other config options.

## Running tests

Install dev requirements:
```
virtualenv venv
venv/bin/pip install -r dev-requirements.txt
venv/bin/pip install tox
```

Run tests:
```
tox
```

## Credits

Thank you to the WakaTime developers and contributors for writing the CLI tool.

